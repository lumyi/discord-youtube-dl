
import re, datetime

timestamp = re.compile("\d+:\d+:\d+.\d+ --> \d+:\d+:\d+.\d+")
dropme = re.compile("(</c>|<c>|<\d+:\d+:\d+.\d+>)")

def should_update_local(line, local):
  h, m, s = line.split()[0].split(":")
  s = s.split(".")[0]
  ref = datetime.timedelta(hours=int(h), minutes=int(m), seconds=0)
  if ref > local:
    return True
  return False

def replace_drop(matchobj):
  for i in dropme.findall(matchobj):
    matchobj = matchobj.replace(i, "")
  return matchobj

def clean_subtitle(text):
  new_text = []
  local_timestamp = datetime.timedelta()
  for line in text:
    if timestamp.match(line):
      if should_update_local(line, local_timestamp):
        local_timestamp += datetime.timedelta(minutes=1)
        line = "%s :" % str(local_timestamp)
      else:
        line = ""
    elif '<' in line:
      line = replace_drop(line)
    if len(line) and line != "\n":
      new_text.append(line)
  return new_text

def trim_subtitle(text):
  new_text = []
  text = clean_subtitle(text)
  previous_line = ""
  for i in text:
    i = i.strip()
    if i == "\n" or i == "" or i == previous_line:
      continue
    else:
      new_text.append(i)
      previous_line = i
  return new_text

if __name__ == '__main__':
  import sys
  with open(sys.argv[1], "r", encoding="utf-8") as f:
    trim_subtitle(f.readlines())
